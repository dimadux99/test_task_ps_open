from marshmallow import fields, Schema
from marshmallow.validate import Range


MIN_VALUE = 1
MIN_OPEN_CELLS = 0
MAX_VALUE = 100
MIN_TURN_VALUE = MIN_VALUE - 1
MAX_TURN_VALUE = MAX_VALUE - 1


class BoardInitSchema(Schema):
    grid_size = fields.Int(required=True, validate=Range(MIN_VALUE, MAX_VALUE))
    bomb_count = fields.Int(required=True, validate=Range(min=MIN_VALUE))
    discover_radius = fields.Int(required=True, validate=Range(min=MIN_VALUE))
    open_cells_count = fields.Int(required=True, validate=Range(min=MIN_OPEN_CELLS))


class TurnSchema(Schema):
    x = fields.Int(required=True, validate=Range(MIN_TURN_VALUE, MAX_TURN_VALUE))
    y = fields.Int(required=True, validate=Range(MIN_TURN_VALUE, MAX_TURN_VALUE))
