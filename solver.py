import json
import random
from itertools import product
from collections import defaultdict
from typing import List, Optional, NoReturn

import numpy as np

from client import Client
from typings import Coords, ServerResponseCoords


class Solver:
    """
    A class to solve mine sweeper

    Attributes
    ----------
    server_url - url string for server connection
    grid_size - count of cells for one side of square grid
    bomb_count - count of bombs to spawn in grid
    discover_radius - maximal distance to closest bomb to show
    open_cells_count - count of opened cells on game start
    """

    UNKNOWN = "?"
    EMPTY = " "
    BOMB = "X"
    DETECT = "V"

    DEFINITELY_NOT_BOMB = -1000
    DEFINITELY_BOMB = 100

    def __init__(self,
                 server_url: str,
                 grid_size: int = 10,
                 bomb_count: int = 10,
                 discover_radius: int = 3,
                 open_cells_count: int = 5):

        self.grid_size = grid_size
        self.bomb_count = bomb_count
        self.discover_radius = discover_radius
        self.open_cells_count = open_cells_count

        self.client = Client(server_url)

        start_cells = self.client.start_game(
            grid_size,
            bomb_count,
            discover_radius,
            open_cells_count
        )
        self.board_state = self._init_board_state(start_cells)

    def _init_board_state(self,
                          start_cells: List[ServerResponseCoords]
                          ) -> np.array:
        """Create user board_state and reveal start cells
            :param start_cells: Sequence of coordinates and value in cell
            :return: Ininted board_state with opened cells
        """
        board = np.full((self.grid_size, self.grid_size), Solver.UNKNOWN)
        for cell in start_cells:
            coords = cell["x"], cell["y"]
            cell_value = cell["value"] or Solver.EMPTY
            board[coords] = cell_value
        return board

    def _update_state(self, coords: Coords, value: Optional[str]):
        """Update board_state with given coords and new value.
           Print updated board_state
            :param coords: coordinates (x,y) of cell to update
            :param value: value to update in cell
            :return: None
        """
        self.board_state[coords] = value or Solver.EMPTY
        print(self.board_state)

    def get_cell_score(self, x: int, y: int) -> np.array:
        """Update probability matrix from current cell value
            :param x: coordinate x,
            :param y: coordinate y,
            :param proba_state: given matrix of probabilities
            :return: updated matrix of probabilities
        """
        result = []

        for radius in range(1, self.discover_radius+1):
            x_start, x_end = max(0, x - radius), x + radius + 1
            y_start, y_end = max(0, y - radius), y + radius + 1
            sub_matrix = self.board_state[x_start:x_end, y_start:y_end]
            symbols = sub_matrix.flatten().tolist()

            if self.board_state[x, y] == Solver.DETECT:
                if any([symbol == str(radius) for symbol in symbols]):
                    unknown_indexes = zip(*np.where(sub_matrix == Solver.UNKNOWN))
                    for x_add, y_add in unknown_indexes:
                        x_update = x_start + x_add
                        y_update = y_start + y_add
                        proba_update = -1 * symbols.count(str(radius)) / len(symbols)
                        result.append((x_update, y_update, proba_update))

            elif self.board_state[x, y] == Solver.UNKNOWN:
                values = [self.discover_radius if symb == Solver.EMPTY else int(symb)
                          for symb in symbols
                          if symb not in [Solver.UNKNOWN, Solver.DETECT]]
                score = 0
                if any([value > radius for value in values]):
                    score = Solver.DEFINITELY_NOT_BOMB
                if any([value == radius for value in values]):
                    score += values.count(radius) / len(symbols)
                if len(values) == 0:
                    score += 1 / (self.grid_size * self.grid_size)
                result.append((x, y, score))

            else:
                values = [symb
                          for symb in symbols
                          if symb not in [Solver.UNKNOWN, Solver.DETECT]]
                if len(symbols)-len(values) == 1 and self.board_state[(x, y)] == str(radius):
                    unknown_indexes = zip(*np.where(sub_matrix == Solver.UNKNOWN))
                    for x_add, y_add in unknown_indexes:
                        result.append((x_start + x_add, y_start + y_add, Solver.DEFINITELY_BOMB))
                        self.board_state[(x_start + x_add, y_start + y_add)] = Solver.DETECT
        return result

    def choose_best_turn(self) -> Coords:
        """Create matrix of probabilities of finding bomb in each cell
            :return: Coordinates of best move for given board state
        """
        probas = []
        for x, y in product(range(self.grid_size), range(self.grid_size)):
            probas = probas + self.get_cell_score(x, y)

        proba_state = defaultdict(float)
        for x, y, value in probas:
            proba_state[(x, y)] += value

        best_indexes = []
        for x, y in product(range(self.grid_size), range(self.grid_size)):
            if self.board_state[x, y] == Solver.UNKNOWN:
                best_indexes.append(((x, y), proba_state[(x, y)]))

        best_indexes = [coords for coords, score in best_indexes
                        if score == min(best_indexes, key=lambda val: val[1])[1]]

        return random.choice(best_indexes)

    def play_game(self) -> NoReturn:
        """Generate a sequence of best moves to solve mine sweeper.
            :return: None
        """
        while True:
            x, y = self.choose_best_turn()
            data = self.client.make_turn(x, y)
            if data["success"]:
                self._update_state((x, y), data["cell_value"])
            else:
                self._update_state((x, y), Solver.BOMB)
                self.client.end_game()
                print("END OF GAME, YOU LOOSE")
                break

            flatten_state = self.board_state.flatten().tolist()
            hidden_cells = flatten_state.count(self.UNKNOWN) + flatten_state.count(self.DETECT)
            if hidden_cells == self.bomb_count:
                self.client.end_game()
                print("END OF GAME, YOU WIN")
                break


if __name__ == "__main__":

    with open("resources/config.json", "r") as config_file:
        config = json.load(config_file)

    solver = Solver(**config)
    solver.play_game()


