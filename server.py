import json
import uuid

from flask import Flask, request, jsonify

from grid import Grid
from marshmallow import ValidationError
from schemas import BoardInitSchema, TurnSchema
from exceptions import InappropriateParametersError

app = Flask(__name__)

app.games = {}
app.grid = None


@app.route('/game', methods=["POST"])
def start_game():
    """
    API to create grid with given parameters
    ---
    parameters:
    - grid_size: count of cells for one side of square grid
      bomb_count: count of bombs to spawn in grid
      discover_radius: maximal distance to closest bomb to show
      open_cells_count: count of opened cells on game start
    responses:
      200:
        description: Successfully created game
        schema:
            game_id: unique identifier of created game
            start_cells: sequence of opened cells and its values
    """
    params = json.loads(request.data)

    try:
        params = BoardInitSchema().load(params)
    except ValidationError as err:
        return jsonify({
            "error": err.messages
        }), 400

    try:
        grid = Grid(**params)
        game_id = uuid.uuid4().hex
        app.games[game_id] = grid
        app.grid = grid
        start_opened_cells = grid.get_start_open_cells()
        return jsonify({
            "id": game_id,
            "start_cells": start_opened_cells
        }), 200
    except InappropriateParametersError as err:
        return jsonify({
            "error": err.messages
        }), 400


@app.route("/game/<game_id>", methods=["POST"])
def make_turn(game_id: str):
    """
    API to make user`s turn. Return value of cell.
    ---
    parameters:
      - x: coordinate x of cell to reveal
        y: coordinate y of cell to reveal
    responses:
      200:
         description: Successfully made turn.
         schema:
           success: Is revealed cell a bomb or not. False - opened a bomb
           cell_value: Closest distance to bomb or 0, if revealed cell is a bomb
    """
    params = json.loads(request.data)
    try:
        params = TurnSchema().load(params)
    except ValidationError as err:
        return jsonify({
            "error": err.messages
        }), 400

    coords = params["x"], params["y"]
    try:
        cell_result = app.games[game_id].make_turn(coords)
    except InappropriateParametersError as err:
        return jsonify({
            "error": err.messages
        }), 400

    return jsonify({
        "success": cell_result[0],
        "cell_value": cell_result[1]
    }), 200


@app.route("/game/<game_id>", methods=["DELETE"])
def end_game(game_id):
    """
    API to delete finished game.
    ---
      responses:
        200:
           description: Successfully deleted game
           schema:
                success: True
        404:
           description: Game by provided game_id not found
           schema:
                success: False
                error: Error message
    """
    try:
        del app.games[game_id]
        return jsonify({
            "success": True
        }), 200
    except KeyError:
        return jsonify({
            "success": False,
            "error": "Game not found"
        }), 404


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
