import unittest

from grid import Grid
from exceptions import InappropriateParametersError

class GridTest(unittest.TestCase):

    def setUp(self) -> None:
        self.grid = Grid(grid_size=5, bomb_count=5, discover_radius=2, open_cells_count=2)

    def test_bomb_counts(self):
        self.assertEqual(self.grid.bomb_matrix.sum(), self.grid.bomb_count)

    def test_open_cells_count(self):
        self.assertEqual(len(self.grid.get_start_open_cells()), self.grid.open_cells_count)

    def test_wrong_turn(self):
        with self.assertRaises(InappropriateParametersError):
            self.grid.make_turn((6, 6))

    def test_right_turn(self):
        result = self.grid.make_turn((3, 3))
        self.assertIsInstance(result[0], bool)
        self.assertIn(result[1], [None] + list(range(self.grid.discover_radius + 1)))

if __name__ == '__main__':
    unittest.main()
