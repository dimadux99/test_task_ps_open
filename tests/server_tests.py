import unittest
import requests
import json
from copy import copy


class ServerTest(unittest.TestCase):

    def setUp(self) -> None:
        with open("../resources/config.json") as file:
            config = json.load(file)

        self.server_url = config["server_url"]
        self.start_url = "{0}/game".format(self.server_url)
        self.turn_url = "{0}/game/{{0}}".format(self.server_url)
        self.end_url = "{0}/game/{{0}}".format(self.server_url)

        self.right_config = {
            "grid_size": 10,
            "bomb_count": 5,
            "discover_radius": 2,
            "open_cells_count": 5
        }

        self.right_turn = {
            "x": 2,
            "y": 2
        }

        self.game_ids = []

    def create_right_game(self):
        game_response = requests.post(
            url=self.start_url,
            data=json.dumps(self.right_config)
        )

        game_id = game_response.json()["id"]
        self.game_ids.append(game_id)
        return game_id

    def test_game_create_right(self):
        response = requests.post(
            url=self.start_url,
            data=json.dumps(self.right_config)
        )

        self.assertEqual(response.status_code, 200)
        self.assertIn("id", response.json())
        self.assertEqual(len(response.json()["start_cells"]), self.right_config["open_cells_count"])
        self.game_ids.append(response.json()["id"])

        for cell in response.json()["start_cells"]:
            self.assertIn("x", cell)
            self.assertIn("y", cell)
            self.assertIn("value", cell)
            if cell["value"]:
                self.assertIn(cell["value"], list(range(self.right_config["discover_radius"] + 1)))

    def test_create_fail(self):
        for parameter in self.right_config:
            config = copy(self.right_config)
            del config[parameter]

            response = requests.post(
                url=self.start_url,
                data=json.dumps(config)
            )

            self.assertEqual(response.status_code, 400)
            self.assertIn("error", response.json())
            self.assertIn(parameter, response.json()["error"])

    def test_create_greater_values(self):
        for parameter in self.right_config:
            config = copy(self.right_config)
            config[parameter] = config[parameter] * 30

            response = requests.post(
                url=self.start_url,
                data=json.dumps(config)
            )

            self.assertEqual(response.status_code, 400)
            self.assertIn("error", response.json())
            self.assertIn(parameter, response.json()["error"])

    def test_make_turn(self):
        game_id = self.create_right_game()
        turn_response = requests.post(
            url=self.turn_url.format(game_id),
            data=json.dumps(self.right_turn)
        )

        self.assertEqual(turn_response.status_code, 200, "Should be created")
        self.assertEqual(turn_response.json()["success"], True)
        self.assertIn("cell_value", turn_response.json())

    def test_wrong_turn(self):
        game_id = self.create_right_game()

        wrong_turns = [
            {"x": 100, "y": 100},
            {"x": 30, "y": 50},
            {"x": "sing", "y": "wing"},
            {"x": 1},
        ]

        for wrong_turn in wrong_turns:
            turn_response = requests.post(
                url=self.turn_url.format(game_id),
                data=json.dumps(wrong_turn)
            )

            self.assertEqual(turn_response.status_code, 400)
            self.assertIn("error", turn_response.json())

    def test_right_delete(self):
        game_id = self.create_right_game()
        response = requests.delete(self.end_url.format(game_id))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["success"], True)

    def test_wrong_delete(self):
        game_id = "THIS ID DOES NOT EXIST"
        response = requests.delete(self.end_url.format(game_id))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json()["success"], False)

    def tearDown(self) -> None:
        for game_id in self.game_ids:
            requests.delete(self.end_url.format(game_id))





if __name__ == '__main__':
    unittest.main()
