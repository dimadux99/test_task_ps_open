import unittest
import json

from client import Client
from exceptions import ServerResponseError

class ClientTests(unittest.TestCase):

    def setUp(self) -> None:
        with open("../resources/config.json") as file:
            config = json.load(file)
        self.client = Client(config["server_url"])
        self.right_config = dict(
            grid_size=5,
            bomb_count=2,
            discover_radius=3,
            open_cells_count=5,
        )
        self.client.start_game(**self.right_config)

    def test_start_game_right(self):
        result = self.client.start_game(**self.right_config)
        self.assertEqual(len(result), self.right_config["open_cells_count"])

    def test_start_game_wrong(self):
        config = dict(
            grid_size=5,
            bomb_count=1,
            discover_radius=1,
            open_cells_count=30,
        )

        with self.assertRaises(ServerResponseError):
            self.client.start_game(**config)

    def test_turn_right(self):
        right_turn = dict(
            x=1,
            y=1,
        )

        result = self.client.make_turn(**right_turn)
        self.assertEqual(result["success"], True)

    def test_turn_wrong(self):
        wrong_turn = dict(
            x=49,
            y=30
        )

        with self.assertRaises(ServerResponseError):
            self.client.make_turn(**wrong_turn)

    def test_delete_game(self):
        self.client.start_game(**self.right_config)
        result = self.client.end_game()
        self.assertEqual(result["success"], True)

    def tearDown(self) -> None:
        self.client.end_game()


if __name__ == '__main__':
    unittest.main()
