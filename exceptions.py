class TestTaskException(Exception):
    pass


class InappropriateParametersError(TestTaskException):

    def __init__(self, field, messages):
        self.messages = {field: messages}


class ServerResponseError(TestTaskException):
    pass
