import random
import numpy as np
from typing import Tuple, List, Dict

from exceptions import InappropriateParametersError
from typings import Coords


class Grid:
    """
    Class to represent hidden bomb_matrix and analyze user`s moves

    Attributes
    ----------
    grid_size - count of cells for one side of square grid
    bomb_count - count of bombs to spawn in grid
    discover_radius - maximal distance to closest bomb to show
    open_cells_count - count of opened cells on game start
    """

    def __init__(self, grid_size, bomb_count, discover_radius, open_cells_count):
        self.grid_size = grid_size
        self.bomb_count = bomb_count
        self.discover_radius = discover_radius
        self.open_cells_count = open_cells_count

        if self.bomb_count > self.grid_size * self.grid_size:
            raise InappropriateParametersError("bomb_count", ["Should be less than count of cells"])
        if self.open_cells_count > self.grid_size * self.grid_size - self.bomb_count:
            raise InappropriateParametersError("open_cells_count", ["Should be less than count of cells"])
        if self.discover_radius > self.grid_size:
            raise InappropriateParametersError("discover_radius", ["Should be less than grid size"])

        self.bomb_matrix = self.generate_grid()

    def _get_random_cell(self) -> Coords:
        """Generate random cell coordinates in given grid
            :return: Coordinate of random cell
        """
        return (
            random.randint(0, self.grid_size-1),
            random.randint(0, self.grid_size-1)
        )

    def get_start_open_cells(self) -> List[Dict[str, int]]:
        """Generate sequence of non-bomb random cells with size of open_cells_count attr
            :return: Sequence of random cells
        """
        cells = []
        unique_coords = set()

        while len(cells) != self.open_cells_count:
            coords = self._get_random_cell()
            if coords not in unique_coords:
                opened_cell = self.make_turn(coords)
                if opened_cell[0]:
                    cells.append({
                        "x": coords[0],
                        "y": coords[1],
                        "value": opened_cell[1]
                    })
        return cells

    def generate_grid(self) -> np.array:
        """Generate grid with given parameters: grid_size, bomb_count
            :return: Numpy matrix with bomb coordinates
        """
        grid_matrix = np.zeros((self.grid_size, self.grid_size))
        bomb_indexes = set()
        while len(bomb_indexes) != self.bomb_count:
            bomb = self._get_random_cell()
            bomb_indexes.add(bomb)
        for bomb in bomb_indexes:
            grid_matrix[bomb] = 1
        return grid_matrix

    def make_turn(self, coords: Coords) -> Tuple[bool, int]:
        """Compute value of provided by user coords
            :param coords: x,y of user move
            :return: boolean game state(False if Finished), represent value of cell
        """
        if coords[0] > self.grid_size - 1:
            raise InappropriateParametersError("x", ["Should be less than grid_size"])
        if coords[1] > self.grid_size - 1:
            raise InappropriateParametersError("y", ["Should be less than grid_size"])

        if self.bomb_matrix[coords] > 0:
            success = False
            closest_bomb_distance = 0
        else:
            closest_bomb_distance = None
            x, y = coords
            for radius in range(1, self.discover_radius+1):
                x_start, x_end = max(0, x - radius), x + radius + 1
                y_start, y_end = max(0, y - radius), y + radius + 1
                sub_matrix = self.bomb_matrix[x_start:x_end, y_start:y_end]
                if sub_matrix.sum() > 0:
                    closest_bomb_distance = radius
                    break
            success = True
        return success, closest_bomb_distance
