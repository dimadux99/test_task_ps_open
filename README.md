#Description

server.py - realises all server-side logic and has 3 endpoints:
* /game [POST] - creates game by provided parameters
* /game/<game_id> [POST] - returns value of cell by provided coordinates
* /game/<game_id> [DELETE] - deletes game by provided game identifier

grid.py - realises all grid logic, as generating grid with provided parameters and computing cell values
client.py - realises interface for client-server communication. It has 3 methods for each endpoint, and wraps erros
solver.py - realises algorithm to choose optimal cells to reveal

There is requirements.txt with all necessary packages
You could launch the game by following next steps:
1. virtualenv python=python3 venv
2. source venv/vin/activate
3. pip3 install -r requirements.txt
4. python server.py
5. python solver.py

#Solver Logic
An overall strategy is create probability matrix, where for each cell,
we can compute probability that there is a bomb.
Because of changing the game rules, there could be cases with same probabilities for bomb and empty cell.
General logic is to compute probability by count of cells, that cover target cells with its radius.
For each cell we are going to build submatrices from 1 to discover radius. 
If there is a value in the submatrix greater than the radius of the submatrix, this means that the current value is definitely not a bomb
If for some open value with its submatrix we have only one unknown cell, this means that it should be a bomb, and we mark it as "V".
If we detected a bomb, we could lower probability for all neighbors.