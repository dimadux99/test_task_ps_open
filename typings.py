from typing import Optional, Tuple

try:
    from typing import TypedDict  # >=3.8
except ImportError:
    from mypy_extensions import TypedDict  # <=3.7

Coords = Tuple[int, int]


class ServerResponseCoords(TypedDict):
    x: int
    y: int
    value: Optional[int]
