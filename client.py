import json
from typing import Dict, List
import requests

from exceptions import ServerResponseError
from typings import ServerResponseCoords


class Client:
    """
    A class to communicate with server

    Attributes
    ----------
    server_url - url string for server connection
    """

    def __init__(self, server_url: str):

        self.server_url = server_url

        self.start_url = "{0}/game".format(self.server_url)
        self.turn_url = "{0}/game/{{0}}".format(self.server_url)
        self.end_url = "{0}/game/{{0}}".format(self.server_url)

        self.game_id = None

    def start_game(self,
                   grid_size: int,
                   bomb_count: int,
                   discover_radius: int,
                   open_cells_count: int) -> List[ServerResponseCoords]:
        """Send request to server with given parameters from config.
           Init user board based on server response
            :return: game_id - unique identified of game, board_state - inited user board
        """
        response = requests.post(
            url=self.start_url,
            data=json.dumps({
                "grid_size": grid_size,
                "bomb_count": bomb_count,
                "discover_radius": discover_radius,
                "open_cells_count": open_cells_count
            })
        )
        if response.status_code == 200:
            data = response.json()
            self.game_id = data["id"]
            start_cells = data["start_cells"]
            return start_cells
        else:
            error_message = response.json()["error"]
            raise ServerResponseError(error_message)

    def end_game(self) -> Dict[str, str]:
        """Send request to server to delete current game
            :return: JSON response from server
        """
        response = requests.delete(self.end_url.format(self.game_id))
        return response.json()

    def make_turn(self, x, y):
        response = requests.post(
            url=self.turn_url.format(self.game_id),
            data=json.dumps({
                "x": int(x),
                "y": int(y)
            })
        )

        if response.status_code == 200:
            return response.json()
        else:
            error_message = response.json()["error"]
            raise ServerResponseError(error_message)


